# import selenium module
from selenium import webdriver
import keyboard
import time
from datetime import datetime, timedelta
from typing import List

startTime = 0
endTime = 0
timeLapseSec = 30
# import select class
from selenium.webdriver.support.ui import Select

driver = webdriver.Chrome("C:/Users/mainaya/Downloads/chromedriver_win32/chromedriver.exe")

# web page url and open first window page
driver.get('https://monkeytype.com/')

time.sleep(5) # Let the user actually see something!

class wordStatusPair:
    def __init__(self, word):
        self.word = word
        self.status = False

def writeAndUpdateStatus(wordStatusObj: wordStatusPair):
    if wordStatusObj.status == True:
        return
    for letter in wordStatusObj.word:
        keyboard.write(letter)
        time.sleep(0.04)
    keyboard.write(' ')
    wordStatusObj.status = True

allTextTupleList: List[wordStatusPair] = []
allText = ''

words = driver.find_element_by_id('words')
visibleText = words.text
visibleText = visibleText.replace("\n", " ")
allTextList = visibleText.split(' ')
for word in allTextList:
    allTextTupleList.append(wordStatusPair(word))

timeEnd = datetime.now() + timedelta(seconds=timeLapseSec)
for item in allTextTupleList:
    writeAndUpdateStatus(item)

while(True):
    #fetch new list
    words = driver.find_element_by_id('words')
    visibleText = words.text
    visibleText = visibleText.replace("\n", " ")
    print(visibleText)
    newStringList = visibleText.split(' ')
    indexOfLastWordThatMatches = 0
    for i in range(1, len(newStringList)):
        if allTextTupleList[-1].word == newStringList[-i]:
            indexOfLastWordThatMatches = len(newStringList) + -i
            break

    for i in range(indexOfLastWordThatMatches+1, len(newStringList)):
        allTextTupleList.append(wordStatusPair(newStringList[i]))

    for item in allTextTupleList:
        if item.status == False:
            writeAndUpdateStatus(item)

    if datetime.now() > timeEnd:
        break

time.sleep(20) # Let the user actually see something!

driver.quit()

